from flask import request
from flask import Flask
import Queue
from flask import session
from flask import url_for, redirect
import os
from flask import render_template
from twilio.rest import TwilioRestClient
import twilio.twiml, threading, json, time, datetime
# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText
app=Flask(__name__)
def timing(f):#http://stackoverflow.com/a/5478448/2039735
    def wrap(*args):
        time1 = time.clock()#http://stackoverflow.com/a/85533/2039735
        ret = f(*args)
        time2 = time.clock()
        timeTaken=time2-time1
        #timeTaken=timeit.timeit(f(*args))
        print '%s function took %0.3f ms' % (f.func_name, (timeTaken)*1000.0)
        return ret
    return wrap
q=Queue.Queue()
SITE_ROOT=os.path.realpath(os.path.dirname(__file__))
with open(os.path.join(SITE_ROOT,"twilio.config")) as twilioconfig:
	ACCOUNT_SID=twilioconfig.readline().strip()
	AUTH_TOKEN=twilioconfig.readline().strip()
advisor_hours_start=False
queueOperationLock=threading.Lock()
try:
	client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)
except Exception as e:
	print e;
smsfooter='\n--\nReply\n"x" to leave the queue\n"s" for status\n"p n" to push behind n\n--\nFor eg.: "p 2" will push 2 people ahead of you'
class Student:
	def __init__(self,full_name,uta_id,cell_number,urgent,comment):
		self.full_name=full_name
		self.uta_id=uta_id
		self.cell_number=cell_number
		self.urgent=urgent
		self.comment=comment
@timing
def send_email():
	# Create a text/plain message
	msg = MIMEText("This is my email body")
	# me == the sender's email address
	# you == the recipient's email address
	msg['Subject'] = 'The subject line'
	me='pranay@shirolkar.me'
	msg['From'] = me
	you='pranay.shirolkar@mavs.uta.edu'
	msg['To'] = you
	# Send the message via our own SMTP server, but don't include the
	# envelope header.
	s = smtplib.SMTP('smtp.gmail.com',587)
	s.ehlo()
	s.starttls()#http://stackoverflow.com/a/27515833/2039735
	s.login("pranayshirolkar","mypaswd")
	s.sendmail(me, [you], msg.as_string())
	s.quit()
@timing
def initialize():
	global q;
	global advisor_hours_start
	advisor_hours_start=False
	try:
		SITE_ROOT=os.path.realpath(os.path.dirname(__file__))
		with open(os.path.join(SITE_ROOT,"backup.json")) as json_file:
			data=json.load(json_file)
		i=0
		for student in data:
			std=Student(student["full_name"],student["uta_id"],student["cell_number"],"True"==student["urgent"],student["comment"])
			q.put(std) 
	except:
		print "an error occurred while loading the backup data"
@app.before_request
def before_request():
	print "before_request was called"
@app.route('/sms',methods=['GET','POST'])
@timing
def sms():
	global q;
	from_number=str(request.values.get('From',None))
	from_number=from_number[:2]+" ("+from_number[2:5]+") "+from_number[5:8]+"-"+from_number[8:];
	msg=request.values.get('Body',None).strip()
	message="-\n"
	if(msg=='s' or msg=='S'):
		print "status request sms"
		#look if exists in q and reply with current position/ error
		i=1;
		for student in q.queue:
			if(from_number==student.cell_number):	
				message+="Hi, "+student.full_name+" you are currently #"+ str(i)+" in the queue!";
				break;
			i+=1;
		if(i-1==q.qsize()):
			message+="Sorry, you're not on the queue. Get in the queue at mavq.uta.edu";
	elif(msg=='x' or msg=='X'):
		print "remove request sms"
		#look if exists in q and reply with cnfmtn or /error
		i=1;
		flag=False
		for student in q.queue:
			print student.cell_number
			if (i==1 and advisor_hours_start):
				message+="You've already been called in. You are technically out of the queue anyways. Thanks for the prompt response though!"
				break;
			if(from_number==student.cell_number):
				with queueOperationLock:
					p=q;
					q=Queue.Queue()
					for std in p.queue:
						print std.uta_id
						if not (std.uta_id == student.uta_id):
							q.put(std)
				threading.Thread(target=writeQtoJSON).start()
				message+="We removed you from the queue."; 
				flag=True
				break;
			i+=1;
		if((i-1)==(q.qsize())):
			if not flag:
				message+="You were not on the queue.";
	elif((msg[0]=='p' or msg[0]=='P') and len(msg)>1 and msg[1]==' '):
		print "push request sms"
		#validate n
		try:
			n=int(msg[2:])
			if(n==0):
				raise ValueError('n was input as 0')
			#look if exists in q and reply with new position /error
			i=1;
			flag=False
			for student in q.queue:
				#print student.cell_number
				if (i==1 and advisor_hours_start):
					message+="You've already been called in. It's too late. Sorry."
					break;
				if(from_number==student.cell_number):
					with queueOperationLock:
						p=q;
						q=Queue.Queue()
						startCounting=0;
						j=0;
						for std in p.queue:
							if(n==j):
								q.put(student)
								startCounting=0;
							if(startCounting):
								j+=1;
							print std.uta_id
							if not (std.uta_id == student.uta_id):
								q.put(std)
							else:
								startCounting=1
						if(j<=n and startCounting):
							q.put(student)
					threading.Thread(target=writeQtoJSON).start()
					message+="You were pushed behind "+str(j)+" people. You are now at #"+str(i+j);
					flag=True
					break;
				i+=1;
			if((i-1)==(q.qsize())):
				if not flag:
					message+="Sorry, you're not on the queue. Get in the queue at mavq.uta.edu";
		except:
			message+=smsfooter
	else:
		message+=smsfooter
	resp = twilio.twiml.Response()
	resp.message(message)
	return str(resp)
@app.route('/')
def index():
	return render_template('index.html')
@app.route('/register',methods=['GET'])
def register():
	return render_template('register.html')
@app.route('/dump')
def dump():
	json_data='[\n'
	i=1
	for student in q.queue:
		if(i==1):
			json_data+='{"sno":"'+str(i)+'", "full_name":"'+student.full_name+'", "uta_id": "1*****'+student.uta_id[-4:]+'"}'
		else:
			json_data+=',\n{"sno":"'+str(i)+'", "full_name":"'+student.full_name+'", "uta_id": "1*****'+str(student.uta_id)[-4:]+'"}'
		i+=1
	json_data+='\n]'
	return json_data
@timing
def writeQtoJSON():
	json_data='[\n'
	i=1
	for student in q.queue:
		if(i==1):
			json_data+='{"sno":"'+str(i)+'", "full_name":"'+student.full_name+'", "uta_id": "'+student.uta_id+'", "cell_number": "'+student.cell_number+'", "urgent": "'+str(student.urgent)+'", "comment": "'+student.comment+'"}'
		else:
			json_data+=',\n{"sno":"'+str(i)+'", "full_name":"'+student.full_name+'", "uta_id": "'+student.uta_id+'", "cell_number": "'+student.cell_number+'", "urgent": "'+str(student.urgent)+'", "comment": "'+student.comment+'"}'
		i+=1
	json_data+='\n]'
	#print json_data
	with open(os.path.join(SITE_ROOT,'backup.json'), 'wb') as target:
		target.write(json_data)
@app.route('/displayScreen')
def displayScreen():
	return render_template('displayscreen.html')
@app.route('/register',methods=['POST'])
def doRegister():
	if not (request.form.get('full_name')):
		return render_template('register.html',alert='required')
	if not (request.form.get('uta_id')):
		return render_template('register.html',alert='required')
	if not (request.form.get('cell_number')):
		return render_template('register.html',alert='required')
	if not (request.form.get("uta_id").isdigit()):
		return render_template('register.html',alert='bad-id')
	if not (len(request.form.get("uta_id"))==10):
		return render_template('register.html',alert='bad-id')
	v_comment=request.form.get('comment')
	v_urgent=False
	if (request.form.get('urgent')=='1'):
		v_urgent=True
		if not (v_comment):
			return render_template('register.html',alert='required')
	for student in q.queue:
		if(student.uta_id==request.form.get("uta_id")):
			return render_template('register.html',alert='double-id')
	newinline=Student(request.form.get("full_name"), request.form.get("uta_id"), request.form.get("cell_number"),v_urgent,v_comment)
	#SERVER SIDE VALIDATION IS REQUIRED BECAUSE JAVASCRIPT VALIDATION IS NOT SAFE
	try:
		client.messages.create(
		to=newinline.cell_number, 
		from_="+18176087345", 
		body="-\n Awesome " + newinline.full_name + ", you are placed at #"+ str(q.qsize()+1) +" in the queue."+smsfooter, 
		)
	except Exception as e:
		if( ("21211" in str(e)) or ("21614" in str(e)) ):
			return render_template('register.html',alert='bad-phone-number')
		if("21608" in str(e)):
			print "BECAUSE OF TRIAL A/C Twilio DOESN'T SEND SMS TO UNVERIFIED NUMBERS."+str(e)
			q.put(newinline)
			threading.Thread(target=writeQtoJSON).start()
			return render_template('index.html',alert="problem-unverified")
		else:
			print str(e);
			return render_template('register.html',alert='unknown')
	q.put(newinline)
	threading.Thread(target=writeQtoJSON).start()
	return render_template('index.html',alert="success")
@app.route("/advisorScreen")
def advisorScreen():
	print "advisorScreen called";
	if not(session.get('username')):
		return redirect(url_for('login'))
	if 'alert' in request.args:
		print "ye bheji"
		return render_template("advisorscreen.html",qlist=q.queue,qnourgent=countUrgent(),alert=request.args['alert'])
	return render_template("advisorscreen.html",qlist=q.queue,qnourgent=countUrgent())
def countUrgent():
	i=0
	for student in q.queue:
		if(student.urgent==True):
			i+=1
	return i;
@app.route('/login')
def login():
	return render_template("login.html")
@app.route('/doLogin', methods=["POST"])
def doLogin():
	print "dologin chali" 
	if(request.form.get('username')==request.form.get('password')):
		session['username']=request.form.get('username')
		return redirect(url_for('advisorScreen'))
	else:
		return render_template("login.html",msg="loginFailed")
@app.route('/doLogout')
def doLogout():
	global advisor_hours_start
	session.pop('username',None)
	session.pop('advisorloggedin',None)
	advisor_hours_start=False
	return render_template("index.html")
@app.route('/startAdvising')
def startAdvising():
	global advisor_hours_start
	if not (session.get('username')):
		return "invalid acccess."
	if q.empty():
		return redirect(url_for("advisorScreen",alert="empty"))
	session['advisorloggedin']=1;
	advisor_hours_start=True
	def smssenderworker(q):
		i=0
		for student in q.queue:
			try:
				if(i==0):
					client.messages.create(to=student.cell_number, body="-\n You're up, walk in now!"+"\n---\nYou will now be out of the queue. Thanks and have a great day!", from_="+18176087345",)
				elif(i < 5):
					client.messages.create(to=student.cell_number, body="-\n Reminder: You're # "+str(i)+" in the queue, be READY to walk in!"+smsfooter, from_="+18176087345",)
				else:
					client.messages.create(to=student.cell_number, body="-\n Info: Advisor has started his office hours!"+smsfooter, from_="+18176087345",)
			except:
				print "BECAUSE OF TRIAL A/C Twilio DOESN'T SEND SMS TO UNVERIFIED NUMBERS."
			i += 1
	threading.Thread(target=smssenderworker, args=(q,)).start()
	return render_template("advisorscreen.html",qlist=q.queue)
@app.route('/nextStudent')
def nextStudent():
	global advisor_hours_start
	if not (session.get('username')):
		return "invalid acccess."
	student_got=q.get()
	############writing log to csv
	with open(os.path.join(SITE_ROOT,'advising_log.csv'), 'ab') as target:
		target.write(student_got.full_name+","+student_got.uta_id+","+str(datetime.datetime.now().date())+","+str(datetime.datetime.now().time())[:-7]+","+str(session.get('username'))+"\n")
	threading.Thread(target=writeQtoJSON).start()
	if q.empty():
		session.pop('advisorloggedin',None)
		advisor_hours_start=False
		return redirect(url_for("advisorScreen",alert="empty"))
	else:
		def smssenderworker1(q):
			i=0
			for student in q.queue:
				try:
					if(i==0):
						client.messages.create(to=student.cell_number, body="-\n You're up, walk in now!"+"\n---\nYou will now be out of the queue. Thanks and have a great day!", from_="+18176087345",)
					elif(i < 5):
						client.messages.create(to=student.cell_number, body="-\n Reminder: You're # "+str(i)+" in the queue, be READY to walk in!"+smsfooter, from_="+18176087345",)
					#else:
						#client.messages.create(to=student.cell_number, body="-\n Status: Advisor has started his office hours!", from_="+18176087345",)
				except:
					print "BECAUSE OF TRIAL A/C Twilio DOESN'T SEND SMS TO UNVERIFIED NUMBERS."
				i += 1
		threading.Thread(target=smssenderworker1, args=(q,)).start()
		return render_template("advisorscreen.html",qlist=q.queue)
@app.route('/stopAdvising')
def stopAdvising():
	session.pop('advisorloggedin',None)
	return redirect(url_for('advisorScreen'))
@app.route('/moveapi',methods=["POST"])
def moveapi():#todo: THIS METHOD HAS NOT BEEN VALIDATED
	global q;
	message="-\n"
	if not (session.get('username')):
		return "invalid acccess."
	id=request.form.get('uta_id');
	if(request.form.get("type")=="up"):
		#up request
		try:
			n=int(request.form.get("moveup"));
		except:
			return redirect(url_for('advisorScreen'));
		if(n<1):
			return redirect(url_for('advisorScreen'));
		#todo: VALIDATION
		#look if exists in q and reply with new position /error
		i=1;
		flag=False
		for student in q.queue:
			if(id==student.uta_id):
				with queueOperationLock:
					p=q;
					q=Queue.Queue()
					j=1;
					k=i-n;
					print k;
					if(k<1):
						k=1;
					for std in p.queue:
						if(k==j):
							print student.uta_id;
							q.put(student)
							flag=True
						if not (std.uta_id == student.uta_id):
							q.put(std)
						j+=1;
				threading.Thread(target=writeQtoJSON).start()
				message+="Alert: Your new position in queue is: #"+str(k)+".";
				try:
					client.messages.create(
					to=student.cell_number, 
					from_="+18176087345", 
					body=message)
				except Exception as e:
					print e;
				break;
			i+=1;
		if((i-1)==(q.qsize())):
			if not flag:
				return redirect(url_for('advisorScreen'+'#tablerow'+str(k)));
	if(request.form.get("type")=="to"):
		#to request
		#todo: VALIDATION
		try:
			n=int(request.form.get("moveto"));
		except:
			return redirect(url_for('advisorScreen'));
		if(n<1 or n>=q.qsize()):
			return redirect(url_for('advisorScreen'));
		#todo: VALIDATION
		#look if exists in q and reply with new position /error
		i=1;
		flag=False
		for student in q.queue:
			if(id==student.uta_id):
				with queueOperationLock:
					p=q;
					q=Queue.Queue()
					j=1;
					for std in p.queue:
						print n;
						print j;
						if(n==j):
							q.put(student)
							flag=True
							j+=1
						if not (std.uta_id == student.uta_id):
							q.put(std)
							print std.uta_id
							j+=1;
				threading.Thread(target=writeQtoJSON).start()
				message+="Alert: Your new position in queue is: #"+str(n)+".";
				try:
					client.messages.create(
					to=student.cell_number, 
					from_="+18176087345", 
					body=message)
				except Exception as e:
					print e;
				break;
			i+=1;
		if((i-1)==(q.qsize())):
			if not flag:
				return redirect(url_for('advisorScreen'));
	return redirect(url_for('advisorScreen')+'#tablerow'+str(n))
app.secret_key=os.urandom(24)
if __name__=='__main__':
	initialize()
	app.debug=True
	app.run(host='0.0.0.0', threaded=True)