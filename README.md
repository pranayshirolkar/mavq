# mavQ
Walk-In Queue Manager for graduate advisors and students, built with Python - Flask and Twilio's communication platform.

This application virtualizes queue management for places such as advisors' offices where queue may be very high and a lot of man-hours are wasted. This application allows users (students) to get in the queue using the web app and then interact with the queue using text messages. So, if they need time, they can reply to the service and they can get behind a few people, or leave the queue, and so on.