from flask import Flask, request, redirect
import twilio.twiml
 
twiliod = Flask(__name__)
 
@twiliod.route("/", methods=['GET', 'POST'])
def hello_monkey():
    """Respond to incoming calls with a simple text message."""
 
    resp = twilio.twiml.Response()
    resp.message("Hello, Arya Babu")
    return str(resp)
 
if __name__ == "__main__":
    twiliod.run(debug=True, host='0.0.0.0', port=5001)